# -*- coding: UTF-8 -*-
"""
GUI test tool and automation framework that uses Accessibility (a11y) technologies to communicate with desktop applications.

Authors: Zack Cerza <zcerza@redhat.com>, Ed Rousseau <rousseau@redhat.com>, David Malcolm <dmalcolm@redhat.com>
"""

__author__ = """Zack Cerza <zcerza@redhat.com>,
Ed Rousseau <rousseau@redhat.com>,
David Malcolm <dmalcolm@redhat.com>"""
__version__ = "0.6.90"
__copyright__ = "Copyright © 2005 Red Hat, Inc."
__license__ = "GPL"

import pyatspi
__all__ = ("config", "predicate", "procedural", "tc", "tree", "utils", "errors")
